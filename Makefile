PROJECT_NAME = mlops4stocks
PYTHON_INTERPRETER = python


# Environment variables for project
ENV	:= .env
include $(ENV)


## Install Python Dependencies
install:
	$(PYTHON_INTERPRETER) -m pip install -U pip setuptools wheel
	$(PYTHON_INTERPRETER) -m pip install -r requirements.txt


## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete


## Lint using flake8
lint:
	python -m black src
	python -m isort src
	flake8 src
	mypy src


# Get test data for MOEX "blue chips"
get_test_data:
	python src/data/get_test_data.py

# Get test data for MOEX "blue chips"
get_imoex_data:
	python src/data/get_imoex_data.py


# Remove test data
rm_test_data:
	rm data/raw/test_data.csv

# Remove DVC cache
rm_dvc_cache:
	rm -rf .dvc/cache

# run DVC repro
start_pipe_from_scratch: rm_test_data rm_dvc_cache
	dvc repro

# Create dvc config.local
create_dvc_config_local:
	dvc remote add --local -d s3_minio_cloud s3://dvc
	dvc remote modify --local s3_minio_cloud endpointurl $(S3_MINIO_CLOUD_ENDPOINTURL)
	dvc remote modify --local s3_minio_cloud access_key_id $(S3_MINIO_CLOUD_ACCESS_KEY_ID)
	dvc remote modify --local s3_minio_cloud secret_access_key $(S3_MINIO_CLOUD_SECRET_ACCESS_KEY)

# run experiment
run_exp:
	cat exp/$(file)
	cp exp/$(file) params.yaml
	dvc repro -f