# create_pct_change
import logging
import sys

import click
import pandas as pd

TRAIN_END_DATE = "2022-11-30"
TEST_START_DATE = "2022-12-01"


@click.command()
@click.argument("input_filepath", type=click.Path())
@click.argument("output_filepath_train", type=click.Path())
@click.argument("output_filepath_test", type=click.Path())
def main(input_filepath, output_filepath_train, output_filepath_test):
    logger = logging.getLogger(logger_name)
    logger.info(f"Get data from {input_filepath}")

    # data = pd.read_csv(input_filepath, index_col="Date")

    # input_filepath = "data/raw/imoex_data.csv"
    data = pd.read_csv(input_filepath, index_col="Date")

    # split data
    train_data = data.loc[:TRAIN_END_DATE]
    test_data = data.loc[TEST_START_DATE:]
    # pct_data = data.pct_change()

    train_data.to_csv(output_filepath_train)
    test_data.to_csv(output_filepath_test)

    logger.info(
        f"Store test data to {output_filepath_train}, \
          train data to {output_filepath_test} "
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
