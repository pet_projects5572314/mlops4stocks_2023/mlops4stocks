# create_pct_change
import logging
import sys

import click
import pandas as pd


@click.command()
@click.argument("input_filepath", type=click.Path())
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    logger = logging.getLogger(logger_name)
    logger.info(f"Get data from {input_filepath}")

    data = pd.read_csv(input_filepath, index_col="Date")
    pct_data = data.pct_change()
    pct_data.to_csv(output_filepath)

    logger.info(f"Store pct_change data to {output_filepath}")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
