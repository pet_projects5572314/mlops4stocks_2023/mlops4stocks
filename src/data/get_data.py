import asyncio
import logging
import sys
import time

import aiohttp
import aiomoex  # type:ignore
import click
import pandas as pd


async def get_single_ticker(
    ticker: str,
    start: str,
    end: str,
    session: aiohttp.ClientSession,
    sem: asyncio.Semaphore,
    delay: float,
    interval: int = 24,
    format: str | None = None,
) -> pd.DataFrame:
    """get OHLCV data form MOEX API"""

    async with sem:
        await asyncio.sleep(delay)
        data = await aiomoex.get_market_candles(
            session, security=ticker, interval=24, start=start, end=end
        )
    """coroutine aiomoex.get_market_candles(
            session: aiohttp.client.ClientSession, 
            security: str, 
            interval: int = 24, 
            start: Optional[str] = None, 
            end: Optional[str] = None, 
            market: str = 'shares', 
            engine: str = 'stock') -> List[Dict[str, Union[str, int, float]]]
    """

    df = pd.DataFrame(data)
    df.index = pd.to_datetime(df["end"]).dt.date  # type:ignore
    df.index.name = "Date"

    df.drop(["begin", "end"], axis=1, inplace=True)
    if format in ["open", "close", "high", "low", "value", "volume"]:
        columns = df.columns
        df[ticker] = df[format]
        df.drop(columns, axis=1, inplace=True)

    return df


async def get_combined_close_price(
    tickers: str,
    output_filepath: str,
    start: str,
    end: str,
    n_sessions: int,
    delay: float,
) -> None:
    """Get close price data for tickers"""

    sem = asyncio.Semaphore(n_sessions)

    async with aiohttp.ClientSession() as session:
        raw_ohlcv_list = await asyncio.gather(
            *[
                get_single_ticker(
                    ticker, start, end, session, sem, delay, format="close"
                )
                for ticker in tickers
            ]
        )
    rezult_data = pd.concat(raw_ohlcv_list, axis=1)
    rezult_data.to_csv(output_filepath)

    logger = logging.getLogger(logger_name)
    logger.info(f"Data save to {output_filepath}\n")
    print(rezult_data.tail())


@click.command()
@click.argument("tickers", type=str)
@click.argument("output_filepath", type=click.Path())
@click.option("--start", type=str, default="2020-01-01")
@click.option("--end", type=str, default="2023-03-01")
@click.option("--n_sessions", type=int, default="1")
@click.option("--delay", type=float, default="1.0")
def main(tickers, output_filepath, start, end, n_sessions, delay):
    """Get test data from MOEX API for list of tickers"""

    logger = logging.getLogger(logger_name)
    logger.info(
        f'Start download data from MOEX API for {tickers} interval: "{start}" -> "{end}"'
    )

    tickers = tickers.split(" ")
    begin = time.time()
    asyncio.run(
        get_combined_close_price(
            tickers, output_filepath, start, end, n_sessions, delay
        )
    )
    finish = time.time()
    logger.info(
        f"Finish download data from MOEX API elapced elapsed time {(finish - begin):.2f} seconds"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]

    main()
