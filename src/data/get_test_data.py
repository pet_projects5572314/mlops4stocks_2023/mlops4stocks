import logging
import subprocess

tickers = "ALRS GAZP GMKN LKOH MGNT MTSS NVTK PHOR PLZL ROSN SBER SNGS TATN TCSG YNDX"
start = "2020-01-01"
end = "2023-03-31"
path = "data/raw/test_data.csv"


def main():
    """Get test data from MOEX API for list of tickers"""

    subprocess.run(
        [
            "python",
            "src/data/get_data.py",
            tickers,
            path,
            "--start",
            start,
            "--end",
            end,
        ]
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
