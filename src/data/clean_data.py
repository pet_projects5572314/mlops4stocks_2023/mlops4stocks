import logging
import sys

import click
import pandas as pd


@click.command()
@click.argument("input_filepath", type=click.Path())
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    logger = logging.getLogger(logger_name)
    logger.info(f"Cleaning data from {input_filepath}")

    data = pd.read_csv(input_filepath, index_col="Date")

    # drop columns
    cutoff = 0.01
    nan_col = data.loc[:, data.isna().mean() > cutoff].columns.tolist()
    logger.info(
        f"Find {len(nan_col)} col where Nan is upper {cutoff} in {input_filepath}"
    )

    if nan_col:
        data.drop(nan_col, axis=1, inplace=True)
        logger.info(f"Remove {nan_col} NaN col")

    # drop rows
    nan_row = data.isna().any(axis=1).sum()
    logger.info(f"Find {nan_row} NaN row in {input_filepath}")

    if nan_row:
        data.dropna(axis=0, inplace=True)
        logger.info(f"Remove {nan_row} NaN row")

    logger.info(f"Store clean data to {output_filepath}")
    data.to_csv(output_filepath)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
