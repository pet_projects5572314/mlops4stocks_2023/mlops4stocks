# create_pct_change
import json
import logging
import sys

import click
import pandas as pd
import pypfopt  # type:ignore
from pypfopt import EfficientFrontier, risk_models


def create_gmv_portfolio(data: pd.DataFrame) -> EfficientFrontier:
    "Return model of GMV portfolio"

    S = risk_models.CovarianceShrinkage(data).ledoit_wolf()
    ef = EfficientFrontier(None, S, weight_bounds=(0, 1))
    ef.min_volatility()

    return ef


@click.command()
@click.argument("input_filepath", type=click.Path())
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    logger = logging.getLogger(logger_name)
    logger.info(f"Get data from {input_filepath}")

    data = pd.read_csv(input_filepath, index_col="Date")
    print(data.tail())
    ef = create_gmv_portfolio(data)
    with open(output_filepath, "w") as f:
        json.dump(ef.clean_weights(), f)

    # with open(output_filepath, "wb") as f:
    #     pickle.dump(ef.clean_weights(), f)  # , protocol=pickle.HIGHEST_PROTOCOL

    logger.info(f"Store EfficientFrontier model to {output_filepath}")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
