import logging
import sys
from dataclasses import dataclass, field

import mlflow  # type:ignore
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, ClassifierMixin  # type:ignore
from sklearn.linear_model import LogisticRegression  # type:ignore
from sklearn.metrics import confusion_matrix  # type:ignore
from sklearn.metrics import accuracy_score, precision_score
from sklearn.preprocessing import PolynomialFeatures  # type:ignore
from sklearn.preprocessing import StandardScaler


class RootModel(BaseEstimator, ClassifierMixin, mlflow.pyfunc.PythonModel):
    """This model predict will stock price rise in next week. Model aggregate simple
    binary logistic regression models for each ticker in data.

    Attributes
    ----------
    models : dict
        Logistic model for each ticker

    scalers : dict
        Scaler for each ticker

    metrs : dict
        Metrics for each ticker

    """

    def __init__(
        self, trust_model_by: str = "all", cutoff: float = 0, min_df_len: int = 63
    ):  # ticker_models: list
        if trust_model_by not in ["all", "precision", "accuracy"]:
            raise Exception("trust_model_by get unknown method")
        self.trust_model_by = trust_model_by
        self.cutoff = cutoff
        self.filter: list = []
        self.min_df_len = min_df_len
        self.models: dict = {}
        self.scalers: dict = {}
        self.predictions: dict = {}
        self.raw_predictions: dict = {}
        self.accuracies: dict = {}
        self.precisions: dict = {}
        self.matrices: dict = {}
        self.rez_comp: dict = {}

    def fit(self, data):
        """Take data, fit logistict model and store it in self.models

        Parameters
        ----------
        data : dict([tiker: (pd.Dataframe, int)])
            data with scaled features and target make by transform method
        """

        # before we need run transform method to get scalers for data
        self._fit_models_for_stocks(data)

    def predict(self, data):
        """Take data and transform it to model format

        Parameters
        ----------
        data : pd.DataFrame
            dataframe where index is date and columns is single ticker

        Returns
        -------
        y_pred
            list of predicted values

        """
        tickers_name = list(data.keys())
        self.filter = []
        self.predictions = {}
        self.raw_predictions = {}
        self.accuracies = {}
        self.precisions = {}
        self.matrices = {}
        self.rez_comp = {}

        for ticker in tickers_name:
            X_test, y_test = data[ticker]
            y_pred = self.models[ticker].predict(X_test)
            self.raw_predictions[ticker] = y_pred
            self.accuracies[ticker] = accuracy_score(y_test, y_pred)
            self.precisions[ticker] = precision_score(y_test, y_pred)
            self.matrices[ticker] = confusion_matrix(y_test, y_pred)
            self.rez_comp[ticker] = list(zip(y_test, y_pred))

        self.filter = self._select_robust_predictions()
        # return prediction on last date of data
        return {
            k: v[-1].item() for k, v in self.raw_predictions.items() if k in self.filter
        }

    def transform(self, data: pd.DataFrame) -> dict:
        """Take data and transform it to model format

        Parameters
        ----------
        data : pd.DataFrame
            dataframe where index is date and columns is single ticker

        Returns
        -------
        scaled_features
            dict where key is ticker and value is tuple (pd.DataFrame with
              created scaled features for this ticker, created Target)
            Save scalers in self.scalers in first run

        """
        if len(data) < self.min_df_len:
            raise Exception(
                f"For current indicators DateFrame must have min 65 rows, you got {len(data)}"
            )

        price_by_ticker = self._get_dict_with_close_prices(data)

        return self._get_scaled_features(price_by_ticker)

    def fit_transform(self, X):
        """Run at ones trasform and fit methods
        clean old models, scalers, metrics for fitting
        """
        self.models = {}
        self.scalers = {}
        self.metrs = {}
        return self.fit(self.transform(X))

    def accuracy(self):
        # metrics = self.accuracies.values()
        metrics = [v for k, v in self.accuracies.items() if k in self.filter]
        return sum(metrics) / len(metrics)

    def precision(self):
        # metrics = self.precisions.values()
        metrics = [v for k, v in self.precisions.items() if k in self.filter]
        return sum(metrics) / len(metrics)

    def confusion_matrix(self):
        metrics = [v for k, v in self.matrices.items() if k in self.filter]
        return metrics

    def _resample_close_price_df(
        self, df: pd.DataFrame, period: str = "1W", columns: list = ["date", "Close"]
    ) -> pd.DataFrame:
        df.index = pd.to_datetime(df.index)
        df = df.resample(period).last()
        # df = df.reset_index()
        df.columns = ["Close"]  # type:ignore
        return df

    def _get_dict_with_close_prices(
        self, data, period: str = "1W", columns: list = ["date", "Close"]
    ) -> dict:
        return {
            ticker: self._resample_close_price_df(data[[ticker]], period, columns)
            for ticker in data.columns
        }

    def _create_features_dataset(self, df):
        # Calculate the RSI feature
        delta = df["Close"].diff()
        gain = delta.where(delta > 0, 0)
        loss = -delta.where(delta < 0, 0)
        avg_gain = gain.rolling(window=14).mean()
        avg_loss = loss.rolling(window=14).mean()
        rs = avg_gain / avg_loss
        df["RSI"] = 100 - (100 / (1 + rs))

        # Calculate the MACD and Signal features
        ema_12 = df["Close"].ewm(span=12, adjust=False).mean()
        ema_26 = df["Close"].ewm(span=26, adjust=False).mean()
        df["MACD"] = ema_12 - ema_26
        df["Signal"] = df["MACD"].ewm(span=9, adjust=False).mean()

        # Define the target variable based on the price change between consecutive days
        df["Target"] = np.where(df["Close"].shift(-1) > df["Close"], 1, 0)

        # Remove any rows with missing values
        df.dropna(inplace=True)

        # Define the features and target variable
        X = df[["RSI", "MACD", "Signal"]]  #'date',
        y = df["Target"]

        return X, y

    def _prepare_features(self, X, ticker):
        # Normalize the features (if first run create scaler for ticker)
        if not self.scalers.get(ticker):
            scaler = StandardScaler()
            scaler.fit(X)
            self.scalers[ticker] = scaler

        X = self.scalers[ticker].transform(X)

        # Add interaction terms between all variables
        poly = PolynomialFeatures(degree=2, include_bias=False)
        X_interaction = poly.fit_transform(X)

        return X_interaction

    def _train_model(self, X, y):
        # Train the logistic regression model
        clf = LogisticRegression(random_state=42)
        clf.fit(X, y)  # X_train, y_train

        return clf

    def _get_scaled_features(self, data):
        tickers = list(data.keys())
        rezult_data = {}
        for ticker in tickers:
            X, y = self._create_features_dataset(data[ticker])
            X_prep = self._prepare_features(X, ticker)
            rezult_data[ticker] = (X_prep, y)

        return rezult_data

    def _fit_models_for_stocks(self, data):
        for ticker, df in data.items():
            self.models[ticker] = self._train_model(df[0], df[1])

    def _get_last_prediction(self):
        return {k: v[-1] for k, v in self.predictions.items()}

    def _select_robust_predictions(self):
        metrics = {
            "accuracy": self.accuracies,
            "precision": self.precisions,
        }
        if self.trust_model_by == "all":
            return list(self.accuracies.keys())
        else:
            return [
                k for k, v in metrics[self.trust_model_by].items() if v > self.cutoff
            ]


def main():
    # get data
    input_filepath = "data/raw/imoex_data.csv"
    data = pd.read_csv(input_filepath, index_col="Date")

    # split data
    train_data = data.loc[:"2022-11-30"]
    test_data = data.loc["2022-12-01":]

    # init model
    rm = RootModel()

    # transform data and train model
    # rm.fit_transform(train_data)
    train_data_tr = rm.transform(train_data)
    rm.fit(train_data_tr)

    # Transform data for test
    test_data_tr = rm.transform(test_data)

    # predict on test data
    pred = rm.predict(test_data_tr)

    import pickle

    with open("tmp/data_for_models.pkl", "wb") as handle:
        pickle.dump(rm, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open("tmp/data_for_models.pkl", "rb") as handle:
        rm = pickle.load(handle)

    # print metrics for all model

    print("accuracies: ", "\n", rm.accuracies, "\n")
    print("precisions: ", "\n", rm.precisions, "\n")

    # for k, v in rm.matrices.items():
    #     print(k, "\n", v, "\n")

    # auxillary func to select robust ticker and use it for portfolio
    def select_robust_predictions(accuracies, cutoff):
        return [k for k, v in accuracies.items() if v > cutoff]

    trusted = select_robust_predictions(rm.accuracies, 0.55)
    print(trusted)

    def predict_rise_tickers(predictions, trusted):
        return {k: v[-1] for k, v in predictions.items() if k in trusted if v[-1] == 1}

    to_use_in_portfolio = predict_rise_tickers(rm.predictions, trusted)
    print(to_use_in_portfolio)

    print(f"accuracy: {rm.accuracy()}")
    print(f"precision: {rm.precision()}")
    print(f"test data len: {len(test_data)}")
    print("Predictions: \n", pred)
    print(test_data_tr, type(test_data_tr))


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
