import logging
import os
import pickle
import sys

import click
import pandas as pd


def select_robust_predictions(accuracies, cutoff):
    return [k for k, v in accuracies.items() if v > cutoff]


class InvalidTickersForSelectStock(Exception):
    """Invalid tickers for select stocks"""

    pass


@click.command()
@click.argument("input_filepath", type=click.Path())
@click.argument("input_model", type=click.Path())
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, input_model, output_filepath):
    logger = logging.getLogger(logger_name)
    logger.info(f"Filtering data from {input_filepath}")

    # load data and model
    data = pd.read_csv(input_filepath, index_col="Date")

    # print(input_model, os.getcwd())
    with open(input_model, "rb") as handle:
        rm = pickle.load(handle)

    # use model to predict will stock rise or not
    data_tr = rm.transform(data)
    pred = rm.predict(data_tr)

    # take tickers that predicted to growth
    TICKERS = [k for k, v in pred.items() if v == 1]
    if set(TICKERS).issubset(set(data.columns)):
        logger.info(f"Tickers will stay for portfolio {TICKERS}")
        data[TICKERS].to_csv(output_filepath)
        logger.info(f"Filtered data save to {input_filepath}")
    else:
        raise InvalidTickersForSelectStock


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
