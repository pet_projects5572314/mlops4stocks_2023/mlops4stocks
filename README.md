# mlops4stocks
This is main repo for mlops4stocks project in ODS MLOps and production approach for ML research.

This code is get to work localy for create, test and store pipeline result to 
mlflow server. Also it up remote servces for that purpose (minio s3, postgres, mlflow server)

The models created by this code work with repos:
- [mlops4stock-api](https://gitlab.com/lemikhovalex/mlops4stock-api) (api for get stock portfolios)
- [bot-mlops4stock](https://gitlab.com/pet_projects5572314/bot-mlops4stock) (telegram bot server)
- [monitoring-mlops4stocks](https://gitlab.com/pet_projects5572314/monitoring-mlops4stocks) (monitoring service)


## main schema
![schema](./docs/schema_project.jpg)

## telegram bot example
![bot](./docs/tg_bot_example.jpg)

## Getting started

### Set-up Python

In this project we use `python3.10`.
Here is snippet for venv installation for local development
```
python3.10 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

### Set-up infrastructure locally

For development there is an instruction on how to set up locally
- s3-like storage Minio
- ML-flow & DB
- Portainer

Firstly define your own environment variables
```
cp sample.env .env
vim .env
```
But for most cases the default must be enough.
**Dont forget** to edit your cpu arch for building.
And finally run
```
docker-compose up -d --build --force-recreate
```
